﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Helpers
{
    public static class JsonRequestHelper
    {

        public static wsResult Post(string url, byte[] file)
        {
            using (WebClient wcWS = new WebClient())
            {
                string data = "";

                if (file != null)
                {
                    byte[] dataBytes = wcWS.UploadData(url, file);
                    data = Encoding.ASCII.GetString(dataBytes);
                }
                else
                    data = wcWS.UploadString(url, WebRequestMethods.Http.Post, "").ToUTF8();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                wsResult result = serializer.Deserialize<wsResult>(data);


                return result;
            }
        }

        public static wsResult PostClass(string url, object o)
        {
            using (WebClient wcWS = new WebClient())
            {
                string data = "";

                JavaScriptSerializer s = new JavaScriptSerializer();
                string go = s.Serialize(o);

                wcWS.Encoding = Encoding.UTF8;

                data = wcWS.UploadString(url, WebRequestMethods.Http.Post, go);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                wsResult result = serializer.Deserialize<wsResult>(data);

                return result;
            }
        }

        public static wsResultProcesso Get(string url)
        {
            using (WebClient wcWS = new WebClient())
            {
                string data = "";

                data = wcWS.DownloadString(url).ToUTF8();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                wsResultProcesso result = serializer.Deserialize<wsResultProcesso>(data);

                return result;
            }
        }

        public static List<FileVersionsItem> GetFileList(string url)
        {
            using (WebClient wcWS = new WebClient())
            {
                string data = wcWS.DownloadString(url).ToUTF8();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<FileVersionsItem> result = serializer.Deserialize<List<FileVersionsItem>>(data);

                return result;
            }
        }

        public static List<ProcessoAnexo> GetFileListRequerimento(string url)
        {
            using (WebClient wcWS = new WebClient())
            {
                string data = wcWS.DownloadString(url).ToUTF8();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ProcessoAnexo> result = serializer.Deserialize<List<ProcessoAnexo>>(data);

                return result;
            }
        }

        public static Stream GetFile(string url)
        {
            using (WebClient wcWS = new WebClient())
            {
                byte[] data = wcWS.DownloadData(url);

                Stream result = new MemoryStream(data);
                result.Position = 0;
                
                return result;
            }
        }

        public static DSD.Curso GetCurso(string url)
        {
            using (WebClient wcWS = new WebClient())
            {
                string data = "";

                data = wcWS.DownloadString(url).ToUTF8();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                DSD.Curso result = serializer.Deserialize<DSD.Curso>(data);

                return result;
            }
        }
        
        public static string GetAnoLetivo(string url, NetworkCredential cred)
        {
            using (WebClient wcWS = new WebClient())
            {
                string data = "";

                if (cred != null)
                {
                    wcWS.UseDefaultCredentials = true;
                    wcWS.Credentials = cred;
                }

                data = wcWS.DownloadString(url).ToUTF8();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string result = serializer.Deserialize<string>(data);

                return result;
            }
        }

    }

    public class wsResult
    {
        public bool response { get; set; }
        public string description { get; set; }
    }

    public class wsResultProcesso
    {
        public string estado { get; set; }
        public string autorizado { get; set; }
        public string despacho { get; set; }
        public string nome { get; set; }
        public string fluxo { get; set; }
        public string cargo { get; set; }
        public string login { get; set; }
    }

    public class wsRequestProcesso
    {
        public string curso { get; set; }
        public string assunto { get; set; }
        public string texto { get; set; }
        public string numMec { get; set; }
        public string nome { get; set; }
        public string loginCriadoPor { get; set; }
        public string nomeCriadoPor { get; set; }
    }


}
