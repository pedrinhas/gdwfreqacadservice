﻿using System;
using System.Configuration;

namespace Helpers
{
    public static class DSD
    {
        private static bool development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

        public static string GetEscola(string curso)
        {
            string urlBase = development ? ConfigurationManager.AppSettings["DSD.CursoDetalhes-dev"] : ConfigurationManager.AppSettings["DSD.CursoDetalhes"];

            string ano = ACAD.GetAnoLetivo();

            string urlFull = string.Format("{0}/{1}/{2}",
                urlBase,
                curso,
                ano);

            Curso result = JsonRequestHelper.GetCurso(urlFull);

            return result.siglaEscola;
        }


        public class Curso
        {
            public int id;
            public int codigo;
            public string curso;
            public string logincoordenadorcurso;
            public string codigoDepartamento;
            public string departamento;
            public string logincoordenadordepartamento;
            public string loginvicecoordenadordepartamento;
            public int codigoEscola;
            public string siglaEscola;
            public string escola;
        }
    }    
}
