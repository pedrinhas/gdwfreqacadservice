﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Helpers
{
    public static class ArqDE
    {
        private static bool development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

        public static string InsertInformacao(string curso, string assunto, string texto, string numMec, string nome, string loginCriadoPor, string nomeCriadoPor)
        {
            string urlBase = development ? ConfigurationManager.AppSettings["IArqDE.InsertInformacao-dev"] : ConfigurationManager.AppSettings["IArqDE.InsertInformacao"];

            wsRequestProcesso request_object = new wsRequestProcesso()
            {
                curso = curso,
                assunto = assunto,
                texto = texto,
                numMec = numMec,
                nome = nome,
                loginCriadoPor = loginCriadoPor,
                nomeCriadoPor = nomeCriadoPor
            };

            wsResult result = JsonRequestHelper.PostClass(urlBase, request_object);

            if (result.response)
                return result.description;
            else
                throw new Exception(result.description);
        }

        public static bool InsertAnexo(string numInf, string login, string nomeAnexo, byte[] file)
        {
            string urlBase = development ? ConfigurationManager.AppSettings["IArqDE.InsertAnexo-dev"] : ConfigurationManager.AppSettings["IArqDE.InsertAnexo"];

            string urlFull = string.Format("{0}/{1}/{2}/{3}",
                urlBase,
                numInf,
                login,
                nomeAnexo);

            wsResult result = JsonRequestHelper.Post(urlFull, file);

            if (result.response)
                return result.response;
            else
                throw new Exception(result.description);
        }

        public static string UpdateAnexo(string numInf, string login, string nomeAnexo, byte[] file)
        {
            string urlBase = development ? ConfigurationManager.AppSettings["IArqDE.UpdateAnexo-dev"] : ConfigurationManager.AppSettings["IArqDE.UpdateAnexo"];

            string urlFull = string.Format("{0}/{1}/{2}/{3}",
                urlBase,
                numInf,
                login,
                nomeAnexo);

            wsResult result = JsonRequestHelper.Post(urlFull, file);

            if (result.response)
                return result.description;
            else
                throw new Exception(result.description);
        }

        public static List<FileVersionsItem> GetFileList(string numero)
        {
            string urlBase = development ? ConfigurationManager.AppSettings["IArqDE.GetFileList-dev"] : ConfigurationManager.AppSettings["IArqDE.GetFileList"];

            string urlFull = string.Format("{0}/{1}",
                urlBase,
                numero);

            List<FileVersionsItem> result = JsonRequestHelper.GetFileList(urlFull);

            if (result != null)
                return result;
            else
                throw new Exception("Ocorreu um erro ao adquirir a listagem de documentos.");
        }

        public static Stream GetFile(string numero, string filename, string version)
        {
            if (string.IsNullOrWhiteSpace(version)) version = "1.0";

            string urlBase = development ? ConfigurationManager.AppSettings["IArqDE.GetFile-dev"] : ConfigurationManager.AppSettings["IArqDE.GetFile"];

            string urlFull = string.Format("{0}/{1}/{2}/{3}",
                urlBase,
                numero,
                filename,
                version);

            Stream result = JsonRequestHelper.GetFile(urlFull);

            if (result != null)
                return result;
            else
                throw new Exception("Ocorreu um erro ao adquirir o documento.");
        }

    }
}
