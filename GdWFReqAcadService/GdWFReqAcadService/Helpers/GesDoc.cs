﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace Helpers
{
    public static class GesDoc
    {
        private static readonly bool Development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

        public static string InsertInformacao(string assunto, string texto, string siglaDestinatario, string processo, string idGesDoc)
        {
            var urlBase = Development ? ConfigurationManager.AppSettings["GesDoc.InsertInformacao-dev"] : ConfigurationManager.AppSettings["GesDoc.InsertInformacao"];

            object requestObject = new
            {
                assunto,
                texto,
                siglaDestinatario = "TESTE",//siglaDestinatario,
                processoArqde = processo,
                processoSigacad = idGesDoc
            };

            var result = JsonRequestHelper.PostClass(urlBase, requestObject);

            if (result.response)
                return result.description;

            throw new Exception(result.description);
        }

        public static wsResultProcesso ConsultaProcesso(string processo)
        {
            var urlBase = Development ? ConfigurationManager.AppSettings["GesDoc.GetEstadoRequerimento-dev"] : ConfigurationManager.AppSettings["GesDoc.GetEstadoRequerimento"];

            var urlFull = $"{urlBase}/{processo}";

            var result = JsonRequestHelper.Get(urlFull);

            if (result.estado == "2") //processo concluído
                return result;

            throw new MessageException($"Processo {processo} ainda ativo no GesDoc");
        }

        public static Stream GetFile(string numero)
        {
            string urlBase = Development ? ConfigurationManager.AppSettings["GesDoc.GetFileRequerimento-dev"] : ConfigurationManager.AppSettings["GesDoc.GetFileRequerimento"];

            var urlFull = $"{urlBase}/{numero}";

            var result = JsonRequestHelper.GetFile(urlFull);

            if (result != null)
                return result;

            throw new Exception("Ocorreu um erro ao adquirir o documento.");
        }



        public static List<ProcessoAnexo> GetFileListRequerimento(string numero)
        {
            var urlBase = Development ? ConfigurationManager.AppSettings["GesDoc.GetFileListRequerimento-dev"] : ConfigurationManager.AppSettings["GesDoc.GetFileListRequerimento"];

            var urlFull = $"{urlBase}/{numero}";

            var result = JsonRequestHelper.GetFileListRequerimento(urlFull);

            if (result != null)
                return result.DistinctBy(x => x.anexo).ToList();

            throw new Exception("Ocorreu um erro ao adquirir o documento.");
        }

        public static Stream GetFile(string numero, string anexo)
        {
            var urlBase = ConfigurationManager.AppSettings["GesDoc.GetFile"];

            var urlFull = $"{urlBase}/{numero}/{anexo}";

            var result = JsonRequestHelper.GetFile(urlFull);

            if (result != null)
                return result;

            throw new Exception("Ocorreu um erro ao adquirir o documento.");
        }

    }

    public class ProcessoAnexo
    {
        public string anexo { get; set; }
    }
}
