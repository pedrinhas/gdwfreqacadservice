﻿using Spire.Doc;
using System;
using System.Configuration;
using System.IO;

namespace Helpers
{
    public static class FileGenerator
    {
        private static bool development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

        public static MemoryStream Novo(string idRequerimento, string nome, string numero, string curso, string assunto, string texto, DateTime data)
        {
            Document document = new Document();

            string fileName = development ? ConfigurationManager.AppSettings["modelo_req-dev"] : ConfigurationManager.AppSettings["modelo_req"];

            document.LoadFromFile(fileName, FileFormat.Docx);

            document.Replace("[processo]", idRequerimento, true, true);
            document.Replace("[nome]", nome, true, true);
            document.Replace("[numero]", numero, true, true);
            document.Replace("[curso]", curso, true, true);
            document.Replace("[assunto]", assunto, true, true); // string.IsNullOrWhiteSpace(r.assunto) ? (string.IsNullOrWhiteSpace(r.tipo) ? "-" : r.tipo) : r.assunto, true, true);
            document.Replace("[texto]", texto, true, true);
            document.Replace("[data]", data.ToString(), true, true);

            MemoryStream ms = new MemoryStream();
            document.SaveToStream(ms, FileFormat.PDF);
            ms.Position = 0;

            return ms;
        }
    }
}
