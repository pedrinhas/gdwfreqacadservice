﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using UTAD.Helpers.ErrorLogger.Sentry;

public static class ACAD
{
    private static bool development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

    public static string GetAnoLetivo()
    {
        try
        {
            string url = development ? ConfigurationManager.AppSettings["ACAD.AnoLetivo-dev"] : ConfigurationManager.AppSettings["ACAD.AnoLetivo"];

            NetworkCredential cred = null;

            if (!development)
                cred = new NetworkCredential(ConfigurationManager.AppSettings["ACAD.Username"], ConfigurationManager.AppSettings["ACAD.Password"]);

            string result = JsonRequestHelper.GetAnoLetivo(url, cred);

            return result;
        }
        catch(Exception e) 
        {        
            SentrySdkExtensions.LogWithContext(e);
        }

        return string.Empty;
    }

    public static string GetEmail(string numero)
    {
        try
        {
            string url = string.Format(ConfigurationManager.AppSettings["ACAD.GetEmail"], numero);

            NetworkCredential cred = new NetworkCredential(ConfigurationManager.AppSettings["ACAD.Username"], ConfigurationManager.AppSettings["ACAD.Password"]);

            string result = Get(url, cred);

            return result;
        }
        catch (Exception e)
        {
            SentrySdkExtensions.LogWithContext(e, new { numero });
        }

        return string.Empty;
    }

    public static string Get(string url, NetworkCredential cred)
    {
        using (WebClient wcWS = new WebClient())
        {
            string data = "";

            if (cred != null)
            {
                wcWS.UseDefaultCredentials = true;
                wcWS.Credentials = cred;
            }

            data = wcWS.DownloadString(url).ToUTF8();

            string result = "";

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            result = serializer.Deserialize<EmailSigacad>(data).Email;
            

            return result;
        }
    }

}

public class EmailSigacad
{
    public string Email { get; set; }
    public string Numero { get; set; }
}
