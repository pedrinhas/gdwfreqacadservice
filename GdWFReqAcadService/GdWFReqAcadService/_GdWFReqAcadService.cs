﻿using Helpers;
using UTAD.Helpers.SharePoint;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using UTAD.Helpers.ErrorLogger.Sentry;
using System.Diagnostics;

public class _GdWFReqAcadService : GdWFReqAcadService
{
    static readonly string LOGIN_UTILIZADOR = "SISTEMA";
    static readonly string NOME_UTILIZADOR = "Requerimentos Online";
    static readonly string KEY = "0000";
    static readonly string SEM_TEXTO = "TEXTO NO FICHEIRO";
    static readonly string ANLRT = "ANLRT"; //Análise - Reitoria
    static readonly string ANLSA = "ANLSA"; //Análise - Académicos
    static readonly string TRMN = "TRMN"; //Terminado
    static readonly string CANC = "CANC"; //Cancelado

    public acadResult wsReqAcadClassifica(string idGesDoc, string tipoReq, string utilizador, string user, string key)
    {
        string desc = "";

        try
        {
            if (db.ReqAcadClassifica(Convert.ToInt64(idGesDoc), tipoReq, utilizador, user, key))
            {
                return new acadResult()
                {
                    code = "Ok",
                    description = desc
                };
            }
            else
                throw new Exception("Ocorreu um erro ao classificar");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { idGesDoc, tipoReq, utilizador, user, key });
        }

        return new acadResult()
        {
            code = "Err",
            description = desc
        };
    }

    public acadResultWorkflow wsReqAcadConsultaProcesso(string idGesDoc, string user, string key)
    {
        string desc = "";

        try
        {
            acadReqAluno wf = db.ReqAcadConsultaProcesso(idGesDoc, user, key);
            if (wf != null)
            {
                return new acadResultWorkflow()
                {
                    wf = wf,
                    code = "Ok",
                    description = desc
                };
            }
            else
                throw new Exception("Ocorreu um erro ao aceder ao processo: " + idGesDoc + ".");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { idGesDoc, user, key });
        }

        return new acadResultWorkflow()
        {
            code = "Err",
            description = desc
        };
    }

    public acadResult wsReqAcadDeleteDoc(string handleDoc, string user, string key)
    {
        string desc = "";

        try
        {
            if (db.ReqAcadDeleteDoc(Convert.ToInt64(handleDoc), user, key))
            {
                return new acadResult()
                {
                    code = "Ok",
                    description = desc
                };
            }
            else
                throw new Exception("Ocorreu um erro ao apagar o documento.");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { handleDoc, user, key });
        }

        return new acadResult()
        {
            code = "Err",
            description = desc
        };
    }

    public acadResultWorkflowID wsReqAcadIniciaComTipo(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string tipoReq, string textoReq, string user, string key)
    {
        string desc = "";

        try
        {
            /*
             * criar processo arqde - retorna idArqDE
             * db.ReqAcadIniciaComTipo - com idGesDoc único (long) - gerado no SP - e idArqDE
             * gerar documento do requerimento e inserir no arqde e BD
             */

            string email = ACAD.GetEmail(numero);

            //criar processo ARQDE
            string idArqDE = ArqDE.InsertInformacao(
                $"{nomeCurso} ({curso})",
                tipoReq,
                textoReq,
                numero,
                nomeAluno,
                LOGIN_UTILIZADOR,
                NOME_UTILIZADOR
                );

            //iniciar requerimento em BD
            long idGesDoc = db.ReqAcadIniciaComTipo(numero, nomeAluno, curso, nomeCurso, anoLectivo, tipoReq, textoReq, user, key, idArqDE, email);

            if (idGesDoc != 0)
            {

                string tipoRequerimento = db.Req_Get_TipoRequerimento(tipoReq);

                //gerar e inserir documento
                var ms = FileGenerator.Novo(idArqDE, nomeAluno, numero, $"{nomeCurso} ({curso})", tipoRequerimento, textoReq, DateTime.Now);

                acadResultObjectID insertResult = wsReqAcadUploadDoc(idGesDoc.ToString(), 49, ms.ToArray(),
                    $"{idArqDE}.pdf", "SISTEMA", "0000");

                if (insertResult.code == "Ok")
                {
                    return new acadResultWorkflowID()
                    {
                        code = "Ok",
                        description = desc,
                        idGesdoc = idGesDoc
                    };
                }
                else
                    throw new Exception(insertResult.description);
            }
            else
                throw new Exception("Ocorreu um erro ao gravar o requerimento.");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, nomeAluno, curso, nomeCurso, anoLectivo, tipoReq, textoReq, user, key });
        }

        return new acadResultWorkflowID()
        {
            code = "Err",
            description = desc,
            idGesdoc = 0
        };
    }

    public acadResultWorkflowID wsReqAcadIniciaComTipoComFile(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string tipoReq, byte[] file, string fileName, string user, string key)
    {
        string desc = "";

        try
        {
            if (file.Length == 0)
                throw new Exception("Ocorreu um erro: o ficheiro está vazio");

            //criar processo ARQDE
            string idArqDE = ArqDE.InsertInformacao(
                string.Format("{0} ({1})", nomeCurso, curso),
                tipoReq,
                SEM_TEXTO,
                numero,
                nomeAluno,
                LOGIN_UTILIZADOR,
                NOME_UTILIZADOR
                );

            string email = ACAD.GetEmail(numero);

            //iniciar requerimento em BD
            long idGesDoc = db.ReqAcadIniciaComTipoComFile(numero, nomeAluno, curso, nomeCurso, anoLectivo, tipoReq, user, key, idArqDE, email);

            if (idGesDoc != 0)
            {
                acadResultObjectID insertResult = wsReqAcadUploadDoc(idGesDoc.ToString(), 49, file, fileName, LOGIN_UTILIZADOR, "0000");

                if (insertResult.code == "Ok")
                {
                    return new acadResultWorkflowID()
                    {
                        code = "Ok",
                        description = desc,
                        idGesdoc = idGesDoc
                    };
                }
                else
                    throw new Exception(insertResult.description);

            }
            else
                throw new Exception("Ocorreu um erro ao gravar o requerimento.");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, nomeAluno, curso, nomeCurso, anoLectivo, tipoReq, fileName, user, key });
        }

        return new acadResultWorkflowID()
        {
            code = "Err",
            description = desc,
            idGesdoc = 0
        };
    }

    public acadResultWorkflowID wsReqAcadIniciaSemTipo(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string assunto, string textoReq, string user, string key)
    {
        string desc = "";

        try
        {
            //criar processo ARQDE
            string idArqDE = ArqDE.InsertInformacao(
                string.Format("{0} ({1})", nomeCurso, curso),
                assunto,
                textoReq,
                numero,
                nomeAluno,
                LOGIN_UTILIZADOR,
                NOME_UTILIZADOR
                );

            string email = ACAD.GetEmail(numero);

            //iniciar requerimento em BD
            long idGesDoc = db.ReqAcadIniciaSemTipo(numero, nomeAluno, curso, nomeCurso, anoLectivo, assunto, textoReq, user, key, idArqDE, email);

            if (idGesDoc != 0)
            {
                //gerar e inserir documento
                MemoryStream ms = FileGenerator.Novo(idArqDE, nomeAluno, numero, $"{nomeCurso} ({curso})", assunto, textoReq, DateTime.Now);

                acadResultObjectID insertResult = wsReqAcadUploadDoc(idGesDoc.ToString(), 49, ms.ToArray(),
                    $"{idArqDE}.pdf", "SISTEMA", "0000");

                if (insertResult.code == "Ok")
                {
                    return new acadResultWorkflowID()
                    {
                        code = "Ok",
                        description = desc,
                        idGesdoc = idGesDoc
                    };
                }
                else
                    throw new Exception(insertResult.description);
            }
            else
                throw new Exception("Ocorreu um erro ao gravar o requerimento.");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, nomeAluno, curso, nomeCurso, anoLectivo, assunto, textoReq, user, key });
        }

        return new acadResultWorkflowID()
        {
            code = "Err",
            description = desc,
            idGesdoc = 0
        };
    }

    public acadResultWorkflowID wsReqAcadIniciaSemTipoComFile(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string assunto, byte[] file, string fileName, string user, string key)
    {
        string desc = "";

        try
        {
            if (file.Length == 0)
                throw new Exception("Ocorreu um erro: o ficheiro está vazio");

            //criar processo ARQDE
            string idArqDE = ArqDE.InsertInformacao(
                string.Format("{0} ({1})", nomeCurso, curso),
                assunto,
                SEM_TEXTO,
                numero,
                nomeAluno,
                LOGIN_UTILIZADOR,
                NOME_UTILIZADOR
                );

            string email = ACAD.GetEmail(numero);

            //iniciar requerimento em BD
            long idGesDoc = db.ReqAcadIniciaSemTipoComFile(numero, nomeAluno, curso, nomeCurso, anoLectivo, assunto, user, key, idArqDE, email);

            if (idGesDoc != 0)
            {
                acadResultObjectID insertResult = wsReqAcadUploadDoc(idGesDoc.ToString(), 49, file, fileName, LOGIN_UTILIZADOR, "0000");

                if (insertResult.code == "Ok")
                {
                    return new acadResultWorkflowID()
                    {
                        code = "Ok",
                        description = desc,
                        idGesdoc = idGesDoc
                    };
                }
                else
                    throw new Exception(insertResult.description);

            }
            else
                throw new Exception("Ocorreu um erro ao gravar o requerimento.");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, nomeAluno, curso, nomeCurso, anoLectivo, assunto, fileName, user, key });
        }

        return new acadResultWorkflowID()
        {
            code = "Err",
            description = desc,
            idGesdoc = 0
        };
    }

    public acadResultListObjects wsReqAcadListaDocumentos(string idGesDoc, string user, string key)
    {
        string desc = "";

        try
        {
            List<sigAcadDocument> handles = db.ReqAcadListaDocumentos(idGesDoc, user, key);
            if (handles != null)
            {
                return new acadResultListObjects()
                {
                    handles = handles,
                    code = "Ok",
                    description = desc
                };
            }
            else
                throw new Exception("Ocorreu um erro ao aceder à listagem de documentos com idGesDoc: " + idGesDoc + ".");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { idGesDoc, user, key });
        }

        return new acadResultListObjects
        {
            code = "Err",
            description = desc
        };
    }

    public acadResultListWorkflows wsReqAcadListaParaAluno(string numero, string estado, string user, string key)
    {
        string desc = "";

        try
        {
            List<acadReqAluno> wfs = db.ReqAcadListaParaAluno(estado, numero, user, key);
            if (wfs != null)
            {
                return new acadResultListWorkflows()
                {
                    wfs = wfs,
                    code = "Ok",
                    description = desc
                };
            }
            else
                throw new Exception("Ocorreu um erro ao aceder à listagem de requerimentos com o estado: " + estado + ", e numero: " + numero + ".");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, estado, user, key });
        }

        return new acadResultListWorkflows()
        {
            code = "Err",
            description = desc
        };
    }

    public acadResultListWorkflows wsReqAcadListaPorEstado(string estado, string user, string key)
    {
        string desc = "";

        try
        {
            List<acadReqAluno> wfs = new List<acadReqAluno>();

            if (estado == "PROCSA")
            {
                wfs = db.ReqAcadListaPorEstado("DEF", user, key);
                wfs.AddRange(db.ReqAcadListaPorEstado("INDEF", user, key));
            }
            else
                wfs = db.ReqAcadListaPorEstado(estado, user, key);

            if (wfs != null)
            {
                return new acadResultListWorkflows()
                {
                    wfs = wfs,
                    code = "Ok",
                    description = desc
                };
            }
            else
                throw new Exception("Ocorreu um erro ao aceder à listagem de requerimentos com o estado: " + estado + ".");
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { estado, user, key });
        }

        return new acadResultListWorkflows()
        {
            code = "Err",
            description = desc
        };
    }


    public acadResult wsReqAcadTramita(string idGesDoc, string estado, string texto, string utilizador, string user, string key)
    {
        string desc = "";

        try
        {
            if (estado == ANLSA)
            {
                //verifica se existem processos ativos no gesdoc ao tramitar para anlsa
                if (!db.ReqPermiteNovoProcessoGesDoc(idGesDoc))
                    throw new Exception("Existe um processo ativo no GesDoc para despacho superior");
            }
            else if (estado == ANLRT) //Análise - Reitoria
            {
                //iniciar processo GesDoc
                if (!db.ReqPermiteNovoProcessoGesDoc(idGesDoc))
                    throw new Exception("Existe um processo ativo no GesDoc para despacho superior");

                if (!db.Req_IniciaProcessoDespachoSuperior(Convert.ToInt64(idGesDoc), texto))
                    throw new Exception("Erro ao gravar processo despacho superior");

            }
            else if (estado == TRMN || estado == CANC) //Terminado ou Cancelado
            {
                //concluir processo no ArqDE
                db.ReqTerminaProcessoArqde(db.Req_Get_Id_ArqDE(Convert.ToInt64(idGesDoc)));
            }

            long id = db.ReqAcadTramita(Convert.ToInt64(idGesDoc), estado, texto, utilizador, user, key);

            if (id != 0)
            {
                return new acadResult()
                {
                    code = "Ok",
                    description = desc
                };
            }

            throw new Exception("Ocorreu um erro ao tramitar o requerimento");

        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { idGesDoc, estado, texto, utilizador, user, key });
        }


        return new acadResult()
        {
            code = "Err",
            description = desc
        };
    }

    public acadResultObjectID wsReqAcadUploadDoc(string idGesDoc, int docType, byte[] file, string fileName, string user, string key)
    {
        string desc = "";

        try
        {
            if (file.Length == 0)
                throw new Exception("Ocorreu um erro: o ficheiro está vazio");

            fileName = fileName.Replace(' ', '_');
            fileName = fileName.Replace("+", "");

            //check se insere novo documento ou nova versão
            long idAnexo = db.Req_CheckAnexo(Convert.ToInt64(idGesDoc), fileName);
            if (idAnexo != 0)
                return wsReqAcadUploadVersion(idAnexo.ToString(), "nova versão", file, fileName, user, key);

            long id = 0;

            //buscar idArqDE
            //inserir doc no arqde
            string idArqDE = db.Req_Get_Id_ArqDE(Convert.ToInt64(idGesDoc));

            if (idArqDE != "-")
            {
                bool insertResult = ArqDE.InsertAnexo(idArqDE, LOGIN_UTILIZADOR, fileName, file);

                if (insertResult)
                {
                    //inserir em BD
                    //return id doc
                    id = db.ReqAcadUploadDoc(Convert.ToInt64(idGesDoc), docType, fileName, user, key);

                    if (id != 0)
                    {
                        return new acadResultObjectID()
                        {
                            code = "Ok",
                            description = desc,
                            objectID = id
                        };
                    }
                    else
                        throw new Exception("Ocorreu um erro ao fazer upload do documento");
                }
                else
                    throw new Exception("Ocorreu um erro ao inserir o documento no ArqDE");
            }
            else
                throw new Exception("Ocorreu um erro adquirir o id do processo no ArqDE");
        }
        catch (Exception e)
        {
            desc = $"{fileName} - {e.Message}";

            SentrySdkExtensions.LogWithContext(e, new { idGesDoc, docType, fileName, user, key });
        }

        return new acadResultObjectID()
        {
            code = "Err",
            description = desc,
            objectID = 0
        };
    }

    public acadResultObjectID wsReqAcadUploadVersion(string handleDoc, string comment, byte[] file, string fileName, string user, string key)
    {
        string desc = "";

        try
        {
            if (file.Length == 0)
                throw new Exception("Ocorreu um erro: o ficheiro está vazio");

            fileName = fileName.Replace(' ', '_');

            //handleDoc = id do anexo
            string idGesDoc = db.Req_HandleDoc_Get_Id_GesDoc(Convert.ToInt64(handleDoc)).ToString();

            long id = 0;

            //buscar idArqDE
            //inserir doc no arqde
            string idArqDE = db.Req_Get_Id_ArqDE(Convert.ToInt64(idGesDoc));

            if (idArqDE != "-")
            {
                string versao = ArqDE.UpdateAnexo(idArqDE, LOGIN_UTILIZADOR, fileName, file);
                if (!versao.Contains(".")) versao += ".0";

                //inserir em BD
                //return id doc
                id = db.ReqAcadUploadVersion(Convert.ToInt64(handleDoc), comment, fileName, versao, user, key);

                if (id != 0)
                {
                    return new acadResultObjectID()
                    {
                        code = "Ok",
                        description = desc,
                        objectID = id
                    };
                }
                else
                    throw new Exception("Ocorreu um erro ao fazer upload do documento");
            }
            else
                throw new Exception("Ocorreu um erro adquirir o id do processo no ArqDE");

        }
        catch (Exception e)
        {
            desc = $"{fileName} - {e.Message}";

            SentrySdkExtensions.LogWithContext(e, new { handleDoc, comment, fileName, user, key });
        }

        return new acadResultObjectID()
        {
            code = "Err",
            description = desc,
            objectID = 0
        };
    }


    //**//

    public acadResultPermiteNovoRequerimento wsReqPermiteNovoRequerimento(string numero, string user, string key)
    {
        string desc = "";

        try
        {
            bool permite = db.ReqPermiteNovoRequerimento(numero, user, key);
            return new acadResultPermiteNovoRequerimento()
            {
                code = "Ok",
                description = desc,
                permite = permite
            };

        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, user, key });
        }

        return new acadResultPermiteNovoRequerimento()
        {
            code = "Err",
            description = desc,
            permite = false
        };
    }

    public acadResultListarDocumentos wsReqListarDocumentos(string numero, string user, string key)
    {
        string desc = "";

        try
        {
            string idArqDE = db.Req_Get_Id_ArqDE(Convert.ToInt64(numero));

            List<FileVersionsItem> list = ArqDE.GetFileList(idArqDE);

            return new acadResultListarDocumentos()
            {
                code = "Ok",
                description = desc,
                list = list
            };

        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, user, key });
        }

        return new acadResultListarDocumentos()
        {
            code = "Err",
            description = desc,
            list = null
        };
    }

    public Stream wsReqGetFile(string numero, string filename, string version, string user, string key)
    {
        string desc = "";

        try
        {
            string idArqDE = db.Req_Get_Id_ArqDE(Convert.ToInt64(numero));

            Stream file = ArqDE.GetFile(idArqDE, filename, version);

            return file;
        }
        catch (Exception e)
        {
            desc = e.Message;

            SentrySdkExtensions.LogWithContext(e, new { numero, filename, version, user, key });
        }

        return null;
    }

    public List<RequerimentoSimple> wsReqListarRequerimentos(string numero, string user, string key)
    {
        try
        {
            List<RequerimentoSimple> list = db.Req_ListarRequerimentos(numero);

            return list;

        }
        catch (Exception e)
        {
            SentrySdkExtensions.LogWithContext(e, new { numero, user, key });
        }

        return null;
    }

    public List<RequerimentoSimple> wsReqListarRequerimentosCurso(string numero, string curso, string user, string key)
    {
        try
        {
            List<RequerimentoSimple> list = db.Req_ListarRequerimentosCurso(numero, curso);

            return list;

        }
        catch (Exception e)
        {
            SentrySdkExtensions.LogWithContext(e, new { numero, user, key });
        }

        return null;
    }



    /*
     * 
     * wsReqGetProcessosGesDoc
     * - devolve List<string> de processos activos no gesdoc
     * 
     */
    public List<string> wsReqGetProcessosGesDoc()
    {
        List<string> resultList;

        try
        {
            resultList = db.Req_ListarProcessosAtivos();

        }
        catch (Exception e)
        {
            resultList = null;

            SentrySdkExtensions.LogWithContext(e);
        }

        return resultList;
    }


    /*
     * 
     * wsReqConsultaProcessoGesDoc
     * - executa processo GesDoc e devolve string com resultado a apresentar na consola
     * 
     */
    public string wsReqConsultaProcessoGesDoc(string p)
    {
        string resultString;

        try
        {
            wsResultProcesso result = GesDoc.ConsultaProcesso(p);

            string idGesDoc = db.Req_Get_Id_GesDoc(p);

            string estado = string.Empty;

            switch (result.autorizado)
            {
                //não autorizado - indeferido (INDEF)
                case "0":
                    estado = "INDEF";
                    break;

                //autorizado - deferido (DEF)
                case "1":
                    estado = "DEF";
                    break;

                //arquivado - aguardar informação adicional (INFOAD)
                case "2":
                    estado = "INFOAD";
                    //estado = !result.despacho.ToUpper().Contains("NÃO") && result.despacho.ToUpper().Contains("AUTORIZ") ? "DEF" : "INFOAD";
                    break;

                default:
                    throw new Exception(p + " - Código devolvido incorreto");
            }

            //tramitar processo
            acadResult tramitaResult = wsReqAcadTramita(idGesDoc, estado, result.despacho, result.login, LOGIN_UTILIZADOR, KEY);

            if (tramitaResult.code == "Err")
                throw new Exception(p + " - " + tramitaResult.description);

            //carregar documento final do gesdoc
            Stream gesdocFile = GesDoc.GetFile(p);

            //guardar documento final do gesdoc
            acadResultObjectID uploadResult = wsReqAcadUploadDoc(idGesDoc, 30, gesdocFile.ToArray(), $"{p}.pdf", LOGIN_UTILIZADOR, KEY);

            if (uploadResult.code == "Err")
                throw new Exception(p + " - " + uploadResult.description);

            //carregar outros documentos inseridos a partir do gesdoc para o processo no arqde
            //SharePointTools.Init(ConfigurationManager.AppSettings["sp-user"], ConfigurationManager.AppSettings["sp-password"], ConfigurationManager.AppSettings["sp-domain"]);

            List<ProcessoAnexo> gesdocFileList = new List<ProcessoAnexo>();

            try
            {
                gesdocFileList = GesDoc.GetFileListRequerimento(p);
            }
            catch { }

            //bool development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

            foreach (var f in gesdocFileList)
            {
                if (f.anexo != $"{p}.pdf")
                {
                    var file = GesDoc.GetFile(p, f.anexo).ToArray();

                    /*byte[] file = SharePointTools.DownloadFile(
                        development ? ConfigurationManager.AppSettings["sharepointConfigAddress-dev"] : ConfigurationManager.AppSettings["sharepointConfigAddress"],
                        SharePointTools.GetSharepointCredentials(),
                        ConfigurationManager.AppSettings["listNameProcessos"],
                        p,
                        f.anexo);*/

                    acadResultObjectID uploadGDResult = wsReqAcadUploadDoc(idGesDoc, 30, file, f.anexo, LOGIN_UTILIZADOR, KEY);

                    if (uploadGDResult.code == "Err")
                        throw new Exception(p + " - " + uploadGDResult.description);
                }
            }

            //terminar processo gesdoc em base de dados
            if (!db.Req_TerminaProcessoGesDoc(p, result.despacho))
                throw new Exception(p + " - Erro ao alterar estado do processo em base de dados");


            resultString = $"sucesso : {p}";
        }
        catch (Exception e)
        {
            //erro ou processo não concluído
            resultString = $"erro|info : {e.Message}";

            if (!(e is MessageException))
            {
                SentrySdkExtensions.LogWithContext(e, new { p });
            }
        }

        return resultString;
    }


    /*
     * 
     * a ser chamdo pelo MAESTRO quando o processo estiver concluido no GesDoc
     * public bool wsReqTerminaProcessoGesDoc
     * 
     */
    public List<string> wsReqConsultarProcessosGesDoc()
    {
        List<string> returnList = new List<string>();

        try
        {
            //carregar processos gesdoc ativos
            List<string> processos = db.Req_ListarProcessosAtivos();

            foreach (var p in processos)
            {
                try
                {
                    wsResultProcesso result = GesDoc.ConsultaProcesso(p);

                    string idGesDoc = db.Req_Get_Id_GesDoc(p);

                    string estado = string.Empty;

                    switch (result.autorizado)
                    {
                        //não autorizado - indeferido (INDEF)
                        case "0":
                            estado = "INDEF";
                            break;

                        //autorizado - deferido (DEF)
                        case "1":
                            estado = "DEF";
                            break;

                        //arquivado - aguardar informação adicional (INFOAD)
                        case "2":
                            estado = "INFOAD";
                            break;

                        default:
                            throw new Exception(p + " - Código devolvido incorreto");
                    }

                    //tramitar processo
                    acadResult tramitaResult = wsReqAcadTramita(idGesDoc, estado, result.despacho, result.login, LOGIN_UTILIZADOR, KEY);

                    if (tramitaResult.code == "Err")
                        throw new Exception(p + " - " + tramitaResult.description);

                    //carregar documento final do gesdoc
                    Stream gesdocFile = GesDoc.GetFile(p);

                    //guardar documento final do gesdoc
                    acadResultObjectID uploadResult = wsReqAcadUploadDoc(idGesDoc, 30, gesdocFile.ToArray(), $"{p}.pdf", LOGIN_UTILIZADOR, KEY);

                    if (uploadResult.code == "Err")
                        throw new Exception(p + " - " + uploadResult.description);

                    //carregar outros documentos inseridos a partir do gesdoc para o processo no arqde
                    //SharePointTools.Init(ConfigurationManager.AppSettings["sp-user"], ConfigurationManager.AppSettings["sp-password"], ConfigurationManager.AppSettings["sp-domain"]);

                    List<ProcessoAnexo> gesdocFileList = new List<ProcessoAnexo>();

                    try
                    {
                        gesdocFileList = GesDoc.GetFileListRequerimento(p);
                    }
                    catch { }

                    //bool development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

                    foreach (var f in gesdocFileList)
                    {
                        if (f.anexo != $"{p}.pdf")
                        {
                            var file = GesDoc.GetFile(p, f.anexo).ToArray();

                            /*byte[] file = SharePointTools.DownloadFile(
                                development ? ConfigurationManager.AppSettings["sharepointConfigAddress-dev"] : ConfigurationManager.AppSettings["sharepointConfigAddress"],
                                SharePointTools.GetSharepointCredentials(),
                                ConfigurationManager.AppSettings["listNameProcessos"],
                                p,
                                f.anexo);*/

                            acadResultObjectID uploadGDResult = wsReqAcadUploadDoc(idGesDoc, 30, file, f.anexo, LOGIN_UTILIZADOR, KEY);

                            if (uploadGDResult.code == "Err")
                                throw new Exception(p + " - " + uploadGDResult.description);
                        }
                    }

                    //terminar processo gesdoc em base de dados
                    if (!db.Req_TerminaProcessoGesDoc(p, result.despacho))
                        throw new Exception(p + " - Erro ao alterar estado do processo em base de dados");


                    returnList.Add($"sucesso : {p}");
                }
                catch (Exception e)
                {
                    //erro ou processo não concluído
                    returnList.Add($"erro|info : {e.Message}");

                    if (!(e is MessageException))
                    {
                        SentrySdkExtensions.LogWithContext(e, new { p });
                    }
                }
            }

        }
        catch (Exception e)
        {
            //erro ao listar processos
            returnList.Add($"erro : {e.Message}");

            SentrySdkExtensions.LogWithContext(e);
        }


        return returnList;
    }



    /*******************************************************************/



    /*
     * 
     * wsReqGetProcessosDespachoSuperior
     * - devolve List<string> de processos para despacho superior
     * 
     */
    public List<RequerimentoDespachoSuperior> wsReqGetProcessosDespachoSuperior()
    {
        List<RequerimentoDespachoSuperior> returnList;

        try
        {
            returnList = db.Req_ListarProcessosDespachoSuperior();

        }
        catch (Exception e)
        {
            returnList = null;

            SentrySdkExtensions.LogWithContext(e);
        }

        return returnList;
    }

    /*
     * 
     * wsReqExecutarProcessoDespachoSuperior
     * - executa processo para despacho superior
     * 
     */
    public string wsReqExecutarProcessoDespachoSuperior(RequerimentoDespachoSuperior p)
    {
        string result;

        try
        {
            acadReqAluno req = wsReqAcadConsultaProcesso(p.idGesDoc, NOME_UTILIZADOR, LOGIN_UTILIZADOR).wf;

            Requerimento aux = db.ReqGetPorProcesso(p.idGesDoc);

            string siglaDestinatario = p.siglaDestinatario;
            if (siglaDestinatario == "PE")
                siglaDestinatario = DSD.GetEscola(req.curso);

            string refGesDoc = GesDoc.InsertInformacao(
                $"{p.tipoRequerimento} | {aux.numero} - {aux.nome} | {aux.codCurso} - {aux.nomeCurso}",
                    p.texto,
                    siglaDestinatario,
                    p.idArqde,
                    p.idGesDoc
                );

            if (!db.Req_IniciaProcessoGesDoc(req.idGesdoc, refGesDoc))
                throw new Exception("Erro ao gravar referência do processo GesDoc");

            if (!db.Req_ConcluiProcessoDespachoSuperior(p.id))
                throw new Exception("Erro ao concluir processo despacho superior");

            result = $"sucesso : {p.idGesDoc} - {refGesDoc}";
        }
        catch (Exception e)
        {
            //erro ou processo não concluído
            result = $"erro : {e.Message}";

            SentrySdkExtensions.LogWithContext(e, new { p });
        }

        return result;
    }



    public List<string> wsReqExecutarProcessosDespachoSuperior()
    {
        List<string> returnList = new List<string>();

        try
        {
            //carregar processos gesdoc ativos
            List<RequerimentoDespachoSuperior> processos = db.Req_ListarProcessosDespachoSuperior();

            foreach (var p in processos)
            {
                try
                {
                    acadReqAluno req = wsReqAcadConsultaProcesso(p.idGesDoc, NOME_UTILIZADOR, LOGIN_UTILIZADOR).wf;

                    Requerimento aux = db.ReqGetPorProcesso(p.idGesDoc);

                    string siglaDestinatario = p.siglaDestinatario;
                    if (siglaDestinatario == "PE")
                        siglaDestinatario = DSD.GetEscola(req.curso);

                    string refGesDoc = GesDoc.InsertInformacao(
                        $"{p.tipoRequerimento} | {aux.numero} - {aux.nome} | {aux.codCurso} - {aux.nomeCurso}",
                            p.texto,
                            siglaDestinatario,
                            p.idArqde,
                            p.idGesDoc
                        );

                    if (!db.Req_IniciaProcessoGesDoc(req.idGesdoc, refGesDoc))
                        throw new Exception("Erro ao gravar referência do processo GesDoc");

                    if (!db.Req_ConcluiProcessoDespachoSuperior(p.id))
                        throw new Exception("Erro ao concluir processo despacho superior");

                    returnList.Add($"sucesso : {p.idGesDoc} - {refGesDoc}");
                }
                catch (Exception e)
                {
                    //erro ou processo não concluído
                    returnList.Add($"erro : {e.Message}");

                    SentrySdkExtensions.LogWithContext(e);
                }
            }

        }
        catch (Exception e)
        {
            //erro ao listar processos
            returnList.Add($"erro : {e.Message}");

            SentrySdkExtensions.LogWithContext(e);
        }


        return returnList;
    }


    /*******************************************************************/


    public string GetAnoLetivo()
    {
        return ACAD.GetAnoLetivo();
    }
}

