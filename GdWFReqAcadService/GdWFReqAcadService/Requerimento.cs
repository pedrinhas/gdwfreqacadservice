﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public class Requerimento
    {

        public string numero { get; set; }

        public string nome { get; set; }

        public string idMatricula { get; set; }

        public string nomeCurso { get; set; }

        public string codCurso { get; set; }

        public string curso { get { return string.Format("{0} - {1}", codCurso, nomeCurso); } }
        
        public string assunto { get; set; }
        public string mensagem { get; set; }

        public string idRequerimento { get; set; }

        public string refRequerimento { get; set; }

        public DateTime data { get; set; }

        public string estado { get; set; }

        public string estadoSigla { get; set; }

        public int iTypeID { get; set; }

        public string tipo { get; set; }

        public string email { get; set; }

        public bool emailSet { get; set; }

        public bool permiteNovoRequerimento { get; set; }

        public string idGesDoc { get; set; }
    }