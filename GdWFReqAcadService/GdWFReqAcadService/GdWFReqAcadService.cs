﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;


[ServiceContract(Namespace = "https://api.utad.pt/")]
public interface GdWFReqAcadService
{

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadIniciaSemTipo", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultWorkflowID wsReqAcadIniciaSemTipo(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string assunto, string textoReq, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadTramita", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResult wsReqAcadTramita(string idGesDoc, string estado, string texto, string utilizador, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadListaDocumentos", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultListObjects wsReqAcadListaDocumentos(string idGesDoc, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadListaParaAluno", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultListWorkflows wsReqAcadListaParaAluno(string numero, string estado, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadUploadDoc", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultObjectID wsReqAcadUploadDoc(string idGesDoc, int docType, byte[] file, string fileName, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadClassifica", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResult wsReqAcadClassifica(string idGesDoc, string tipoReq, string utilizador, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadListaPorEstado", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultListWorkflows wsReqAcadListaPorEstado(string estado, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadDeleteDoc", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResult wsReqAcadDeleteDoc(string handleDoc, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadUploadVersion", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultObjectID wsReqAcadUploadVersion(string handleDoc, string comment, byte[] file, string fileName, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadIniciaComTipo", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultWorkflowID wsReqAcadIniciaComTipo(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string tipoReq, string textoReq, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadConsultaProcesso", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultWorkflow wsReqAcadConsultaProcesso(string idGesDoc, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadIniciaSemTipoComFile", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultWorkflowID wsReqAcadIniciaSemTipoComFile(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string assunto, byte[] file, string fileName, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqAcadIniciaComTipoComFile", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultWorkflowID wsReqAcadIniciaComTipoComFile(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string tipoReq, byte[] file, string fileName, string user, string key);


    //**//


    [OperationContract]
    [WebGet(UriTemplate = "wsReqPermiteNovoRequerimento", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultPermiteNovoRequerimento wsReqPermiteNovoRequerimento(string numero, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqListarDocumentos", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    acadResultListarDocumentos wsReqListarDocumentos(string numero, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqGetFile", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    Stream wsReqGetFile(string numero, string filename, string version, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqListarRequerimentos", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    List<RequerimentoSimple> wsReqListarRequerimentos(string numero, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqListarRequerimentosCurso", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    List<RequerimentoSimple> wsReqListarRequerimentosCurso(string numero, string curso, string user, string key);

    [OperationContract]
    [WebGet(UriTemplate = "wsGetAnoLetivo", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    string GetAnoLetivo();

    [OperationContract]
    [WebGet(UriTemplate = "wsReqConsultarProcessosGesDoc", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    List<string> wsReqConsultarProcessosGesDoc();

    [OperationContract]
    [WebGet(UriTemplate = "wsReqExecutarProcessosDespachoSuperior", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    List<string> wsReqExecutarProcessosDespachoSuperior();

    //**//
    [OperationContract]
    [WebGet(UriTemplate = "wsReqConsultarProcessosGesDoc", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    List<string> wsReqGetProcessosGesDoc();

    [OperationContract]
    [WebGet(UriTemplate = "wsReqConsultarProcessosGesDoc", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    string wsReqConsultaProcessoGesDoc(string p);

    [OperationContract]
    [WebGet(UriTemplate = "wsReqGetProcessosDespachoSuperior", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    List<RequerimentoDespachoSuperior> wsReqGetProcessosDespachoSuperior();

    [OperationContract]
    [WebGet(UriTemplate = "wsReqExecutarProcessoDespachoSuperior", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
    string wsReqExecutarProcessoDespachoSuperior(RequerimentoDespachoSuperior p);



}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultFile : acadResult
{

    [DataMember]
    public Stream file { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultListarDocumentos : acadResult
{

    [DataMember]
    public List<FileVersionsItem> list { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class FileVersionsItem
{
    [DataMember]
    public string name { get; set; }
    [DataMember]
    public string version { get; set; }
}


[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultPermiteNovoRequerimento : acadResult
{

    [DataMember]
    public bool permite { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultWorkflowID : acadResult
{

    [DataMember]
    public long idGesdoc { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResult
{

    [DataMember]
    public string code { get; set; }

    [DataMember]
    public string description { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultWorkflow : acadResult
{

    [DataMember]
    public acadReqAluno wf { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultListObjects : acadResult
{

    [DataMember]
    public List<sigAcadDocument> handles { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class sigAcadDocument
{

    [DataMember]
    public DateTime data { get; set; }

    [DataMember]
    public long handle { get; set; }

    [DataMember]
    public string title { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultObjectID : acadResult
{

    [DataMember]
    public long objectID { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadResultListWorkflows : acadResult
{

    [DataMember]
    public List<acadReqAluno> wfs { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class acadReqAluno
{

    [DataMember]
    public string comment { get; set; }

    [DataMember]
    public string curso { get; set; }

    [DataMember]
    public DateTime dataEstado { get; set; }

    [DataMember]
    public string estado { get; set; }

    [DataMember]
    public long idGesdoc { get; set; }

    [DataMember]
    public string numero { get; set; }

    [DataMember]
    public string @ref { get; set; }

    [DataMember]
    public string sigla { get; set; }

    [DataMember]
    public int iTypeID { get; set; }
}

[DataContract(Namespace = "https://api.utad.pt/")]
public class RequerimentoSimple
{

    [DataMember]
    public string assunto { get; set; }

    [DataMember]
    public string texto { get; set; }

    [DataMember]
    public DateTime data { get; set; }

    [DataMember]
    public string estado { get; set; }

    [DataMember]
    public string estadoSigla { get; set; }

    [DataMember]
    public string tipo { get; set; }

    [DataMember]
    public string email { get; set; }

    [DataMember]
    public long idGesDoc { get; set; }

    [DataMember]
    public string cursoCodigo { get; set; }

}

[DataContract(Namespace = "https://api.utad.pt/")]
public class RequerimentoDespachoSuperior
{

    [DataMember]
    public int id { get; set; }
    [DataMember]
    public string texto { get; set; }

    [DataMember]
    public string idGesDoc { get; set; }

    [DataMember]
    public string siglaDestinatario { get; set; }

    [DataMember]
    public string idArqde { get; set; }

    [DataMember]
    public string tipoRequerimento { get; set; }
}