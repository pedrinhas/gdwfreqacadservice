﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        public List<string> wsReqConsultarProcessosGesDoc()
        {
            List<string> returnList = new List<string>();

            try
            {
                //carregar processos gesdoc ativos
                List<string> processos = db.Req_ListarProcessosAtivos();

                foreach (var p in processos)
                {
                    try
                    {
                        wsResultProcesso result = GesDoc.ConsultaProcesso(p);

                        string idGesDoc = db.Req_Get_Id_GesDoc(p);

                        string estado = string.Empty;

                        switch (result.autorizado)
                        {
                            //não autorizado - indeferido (INDEF)
                            case "0":
                                estado = "INDEF";
                                break;

                            //autorizado - deferido (DEF)
                            case "1":
                                estado = "DEF";
                                break;

                            //arquivado - aguardar informação adicional (INFOAD)
                            case "2":
                                estado = "INFOAD";
                                //estado = !result.despacho.ToUpper().Contains("NÃO") && result.despacho.ToUpper().Contains("AUTORIZ") ? "DEF" : "INFOAD";
                                break;

                            default:
                                throw new Exception(p + " - Código devolvido incorreto");
                        }

                        //tramitar processo
                        acadResult tramitaResult = wsReqAcadTramita(idGesDoc, estado, result.despacho, result.login, LOGIN_UTILIZADOR, KEY);

                        if (tramitaResult.code == "Err")
                            throw new Exception(p + " - " + tramitaResult.description);

                        //carregar documento final do gesdoc
                        Stream gesdocFile = GesDoc.GetFile(p);

                        //guardar documento final do gesdoc
                        acadResultObjectID uploadResult = wsReqAcadUploadDoc(idGesDoc, 30, gesdocFile.ToArray(), string.Format("{0}.pdf", p), LOGIN_UTILIZADOR, KEY);

                        if (uploadResult.code == "Err")
                            throw new Exception(p + " - " + uploadResult.description);

                        //carregar outros documentos inseridos a partir do gesdoc para o processo no arqde
                        SharepointTools.Init(ConfigurationManager.AppSettings["sp-user"], ConfigurationManager.AppSettings["sp-password"], ConfigurationManager.AppSettings["sp-domain"]);

                        List<ProcessoAnexo> gesdocFileList = new List<ProcessoAnexo>();

                        try
                        {
                            gesdocFileList = GesDoc.GetFileListRequerimento(p);
                        }
                        catch { }

                        bool development = Convert.ToBoolean(ConfigurationManager.AppSettings["Development"]);

                        foreach (var f in gesdocFileList)
                        {
                            if (f.anexo != string.Format("{0}.pdf", p))
                            {
                                byte[] file = SharepointTools.DownloadFile(
                                    development ? ConfigurationManager.AppSettings["sharepointConfigAddress-dev"] : ConfigurationManager.AppSettings["sharepointConfigAddress"],
                                    SharepointTools.GetSharepointCredentials(),
                                    ConfigurationManager.AppSettings["listNameProcessos"],
                                    p,
                                    f.anexo);

                                acadResultObjectID uploadGDResult = wsReqAcadUploadDoc(idGesDoc, 30, file, f.anexo, LOGIN_UTILIZADOR, KEY);

                                if (uploadGDResult.code == "Err")
                                    throw new Exception(p + " - " + uploadGDResult.description);
                            }
                        }

                        //terminar processo gesdoc em base de dados
                        if (!db.Req_TerminaProcessoGesDoc(p, result.despacho))
                            throw new Exception(p + " - Erro ao alterar estado do processo em base de dados");


                        returnList.Add(string.Format("sucesso : {0}", p));
                    }
                    catch (Exception e)
                    {
                        //erro ou processo não concluído
                        returnList.Add(string.Format("erro|info : {0}", e.Message));
                    }
                }

            }
            catch (Exception e)
            {
                //erro ao listar processos
                returnList.Add(string.Format("erro : {0}", e.Message));
            }


            return returnList;
        }
    }
}
