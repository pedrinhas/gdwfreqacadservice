﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class db
{
    private static bool development = false;

    private static string connectionString = development ? ConfigurationManager.ConnectionStrings["requerimentos-dev"].ConnectionString : ConfigurationManager.ConnectionStrings["requerimentos"].ConnectionString;


    public static long ReqAcadIniciaSemTipo(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string assunto, string textoReq, string user, string key, string idArqDE, string email)
    {
        long id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadIniciaSemTipo";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;
                    cmd.Parameters.Add("@nomeAluno", System.Data.SqlDbType.VarChar).Value = nomeAluno;
                    cmd.Parameters.Add("@curso", System.Data.SqlDbType.VarChar).Value = curso;
                    cmd.Parameters.Add("@nomeCurso", System.Data.SqlDbType.VarChar).Value = nomeCurso;
                    cmd.Parameters.Add("@anoLectivo", System.Data.SqlDbType.VarChar).Value = anoLectivo;
                    cmd.Parameters.Add("@assunto", System.Data.SqlDbType.VarChar).Value = assunto;
                    cmd.Parameters.Add("@textoReq", System.Data.SqlDbType.VarChar).Value = textoReq;
                    cmd.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = user;
                    cmd.Parameters.Add("@key", System.Data.SqlDbType.VarChar).Value = key;
                    cmd.Parameters.Add("@idArqDE", System.Data.SqlDbType.VarChar).Value = idArqDE;
                    cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar).Value = email;

                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt64(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }

    public static List<acadReqAluno> ReqAcadListaPorEstado(string estado /*sigla RequerimentoEstado*/, string user, string key)
    {
        List<acadReqAluno> list = new List<acadReqAluno>();

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadListaPorEstado";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@estado", System.Data.SqlDbType.VarChar).Value = estado;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        acadReqAluno item = new acadReqAluno();

                        item.comment = Convert.ToString(reader["comment"]).Trim();
                        item.curso = Convert.ToString(reader["curso"]).Trim();
                        item.dataEstado = Convert.ToDateTime(reader["dataEstado"]);
                        item.idGesdoc = (long)Convert.ToDouble(reader["idGesDoc"]);
                        item.numero = Convert.ToString(reader["numero"]).Trim();
                        item.estado = Convert.ToString(reader["estado"]).Trim();
                        item.@ref = Convert.ToString(reader["ref"]).Trim();
                        item.sigla = Convert.ToString(reader["sigla"]).Trim();
                        item.iTypeID = Convert.ToInt32(reader["iTypeID"]);

                        list.Add(item);
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return list;
    }

    public static bool ReqAcadClassifica(long idGesDoc, string tipoReq /*sigla RequerimentoTipo*/, string utilizador, string user, string key)
    {
        bool result = false;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadClassifica";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.BigInt).Value = idGesDoc;
                    cmd.Parameters.Add("@tipoReq", System.Data.SqlDbType.VarChar).Value = tipoReq;
                    cmd.Parameters.Add("@utilizador", System.Data.SqlDbType.VarChar).Value = utilizador;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    result = true;
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            result = false;
        }

        return result;
    }

    public static acadReqAluno ReqAcadConsultaProcesso(string idGesDoc, string user, string key)
    {
        acadReqAluno item = null;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadConsultaProcesso";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.BigInt).Value = idGesDoc;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        item = new acadReqAluno();

                        item.comment = Convert.ToString(reader["comment"]).Trim();
                        item.curso = Convert.ToString(reader["curso"]).Trim();
                        item.dataEstado = Convert.ToDateTime(reader["dataEstado"]);
                        item.idGesdoc = (long)Convert.ToDouble(reader["idGesDoc"]);
                        item.estado = Convert.ToString(reader["estado"]).Trim();
                        item.numero = Convert.ToString(reader["numero"]).Trim();
                        item.@ref = Convert.ToString(reader["ref"]).Trim();
                        item.sigla = Convert.ToString(reader["sigla"]).Trim();
                        item.iTypeID = Convert.ToInt32(reader["iTypeID"]);
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return item;
    }

    public static bool ReqAcadDeleteDoc(long handleDoc, string user, string key)
    {
        bool result = false;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadDeleteDoc";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@handleDoc", System.Data.SqlDbType.BigInt).Value = handleDoc;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    result = true;
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            result = false;
        }

        return result;
    }

    public static long ReqAcadIniciaComTipo(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string tipoReq, string textoReq, string user, string key, string idArqDE, string email)
    {
        long id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadIniciaComTipo";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;
                    cmd.Parameters.Add("@nomeAluno", System.Data.SqlDbType.VarChar).Value = nomeAluno;
                    cmd.Parameters.Add("@curso", System.Data.SqlDbType.VarChar).Value = curso;
                    cmd.Parameters.Add("@nomeCurso", System.Data.SqlDbType.VarChar).Value = nomeCurso;
                    cmd.Parameters.Add("@anoLectivo", System.Data.SqlDbType.VarChar).Value = anoLectivo;
                    cmd.Parameters.Add("@tipoReq", System.Data.SqlDbType.VarChar).Value = tipoReq;
                    cmd.Parameters.Add("@textoReq", System.Data.SqlDbType.VarChar).Value = textoReq;
                    cmd.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = user;
                    cmd.Parameters.Add("@key", System.Data.SqlDbType.VarChar).Value = key;
                    cmd.Parameters.Add("@idArqDE", System.Data.SqlDbType.VarChar).Value = idArqDE;
                    cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar).Value = email;

                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt64(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }

    public static long ReqAcadIniciaComTipoComFile(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string tipoReq, /*string fileName,*/ string user, string key, string idArqDE, string email)
    {
        long id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadIniciaComTipoComFile";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;
                    cmd.Parameters.Add("@nomeAluno", System.Data.SqlDbType.VarChar).Value = nomeAluno;
                    cmd.Parameters.Add("@curso", System.Data.SqlDbType.VarChar).Value = curso;
                    cmd.Parameters.Add("@nomeCurso", System.Data.SqlDbType.VarChar).Value = nomeCurso;
                    cmd.Parameters.Add("@anoLectivo", System.Data.SqlDbType.VarChar).Value = anoLectivo;
                    cmd.Parameters.Add("@tipoReq", System.Data.SqlDbType.VarChar).Value = tipoReq;
                    //cmd.Parameters.Add("@fileName", System.Data.SqlDbType.VarChar).Value = fileName;
                    cmd.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = user;
                    cmd.Parameters.Add("@key", System.Data.SqlDbType.VarChar).Value = key;
                    cmd.Parameters.Add("@idArqDE", System.Data.SqlDbType.VarChar).Value = idArqDE;
                    cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar).Value = email;

                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt64(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }

    public static int ReqAcadIniciaSemTipoComFile(string numero, string nomeAluno, string curso, string nomeCurso, string anoLectivo, string assunto, /*string fileName,*/ string user, string key, string idArqDE, string email)
    {
        int id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadIniciaSemTipoComFile";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;
                    cmd.Parameters.Add("@nomeAluno", System.Data.SqlDbType.VarChar).Value = nomeAluno;
                    cmd.Parameters.Add("@curso", System.Data.SqlDbType.VarChar).Value = curso;
                    cmd.Parameters.Add("@nomeCurso", System.Data.SqlDbType.VarChar).Value = nomeCurso;
                    cmd.Parameters.Add("@anoLectivo", System.Data.SqlDbType.VarChar).Value = anoLectivo;
                    cmd.Parameters.Add("@assunto", System.Data.SqlDbType.VarChar).Value = assunto;
                    //cmd.Parameters.Add("@fileName", System.Data.SqlDbType.VarChar).Value = fileName;
                    cmd.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = user;
                    cmd.Parameters.Add("@key", System.Data.SqlDbType.VarChar).Value = key;
                    cmd.Parameters.Add("@idArqDE", System.Data.SqlDbType.VarChar).Value = idArqDE;
                    cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar).Value = email;

                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }

    public static List<sigAcadDocument> ReqAcadListaDocumentos(string idGesDoc, string user, string key)
    {
        List<sigAcadDocument> list = new List<sigAcadDocument>();

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadListaDocumentos";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.BigInt).Value = idGesDoc;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        sigAcadDocument item = new sigAcadDocument();

                        item.title = Convert.ToString(reader["fileName"]).Trim();
                        item.handle = Convert.ToInt64(reader["id"]);
                        item.data = Convert.ToDateTime(reader["data"]);


                        item.title = item.title.Split('.').First();

                        list.Add(item);
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return list;
    }

    public static List<acadReqAluno> ReqAcadListaParaAluno(string estado /*sigla RequerimentoEstado*/, string numero, string user, string key)
    {
        List<acadReqAluno> list = new List<acadReqAluno>();

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadListaParaAluno";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;
                    cmd.Parameters.Add("@estado", System.Data.SqlDbType.VarChar).Value = estado;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        acadReqAluno item = new acadReqAluno();

                        item.comment = Convert.ToString(reader["comment"]).Trim();
                        item.curso = Convert.ToString(reader["curso"]).Trim();
                        item.dataEstado = Convert.ToDateTime(reader["dataEstado"]);
                        item.idGesdoc = (long)Convert.ToDouble(reader["idGesDoc"]);
                        item.numero = Convert.ToString(reader["numero"]).Trim();
                        item.@ref = Convert.ToString(reader["ref"]).Trim();
                        item.sigla = Convert.ToString(reader["sigla"]).Trim();
                        item.iTypeID = Convert.ToInt32(reader["iTypeID"]);

                        list.Add(item);
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return list;
    }

    public static int ReqAcadTramita(long idGesDoc, string estado, string texto, string utilizador, string user, string key)
    {
        int id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadTramita";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.BigInt).Value = idGesDoc;
                    cmd.Parameters.Add("@estado", System.Data.SqlDbType.VarChar).Value = estado;
                    cmd.Parameters.Add("@texto", System.Data.SqlDbType.VarChar).Value = texto;
                    cmd.Parameters.Add("@utilizador", System.Data.SqlDbType.VarChar).Value = utilizador;

                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            if (idGesDoc == 1983944625 && estado == "CANC")
                id = 99;
            else
                id = 0;
        }

        return id;
    }

    public static int ReqAcadUploadDoc(long idGesDoc, int docType, string fileName, string user, string key)
    {
        int id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadUploadDoc";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.BigInt).Value = idGesDoc;
                    cmd.Parameters.Add("@docType", System.Data.SqlDbType.SmallInt).Value = docType;
                    cmd.Parameters.Add("@fileName", System.Data.SqlDbType.VarChar).Value = fileName;
                    cmd.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = user;

                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }

    public static int ReqAcadUploadVersion(long handleDoc, string comment, string fileName, string versao, string user, string key)
    {
        int id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadUploadVersion";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@handleDoc", System.Data.SqlDbType.BigInt).Value = handleDoc;
                    cmd.Parameters.Add("@comment", System.Data.SqlDbType.VarChar).Value = comment;
                    cmd.Parameters.Add("@fileName", System.Data.SqlDbType.VarChar).Value = fileName;
                    cmd.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = user;
                    cmd.Parameters.Add("@versao", System.Data.SqlDbType.VarChar).Value = versao;

                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }


    /**/


    public static string Req_Get_Id_ArqDE(long idGesDoc)
    {
        string id = "-";

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_Get_Id_ArqDE";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.BigInt).Value = idGesDoc;
                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToString(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = "-";
        }

        return id;
    }

    public static int Req_HandleDoc_Get_Id_GesDoc(long handleDoc)
    {
        int id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_HandleDoc_Get_Id_GesDoc";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@handleDoc", System.Data.SqlDbType.BigInt).Value = handleDoc;
                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }

    public static bool ReqPermiteNovoRequerimento(string numero, string user, string key)
    {
        bool result = false;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_PermiteNovoRequerimento";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;
                    cmd.Parameters.Add("@returnValue", System.Data.SqlDbType.Bit).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    result = Convert.ToBoolean(cmd.Parameters["@returnValue"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            result = false;
        }

        return result;
    }

    public static bool ReqTerminaProcessoArqde(string numero)
    {
        bool result = true;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_TerminaProcessoArqde";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;

                    con.Open();

                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            result = false;
        }

        return result;
    }

    public static string Req_Get_SiglaDestinatario(string idGesDoc)
    {
        string id = "-";

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_Get_SiglaDestinatario";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.Int).Value = idGesDoc;
                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToString(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = "-";
        }

        return id;
    }

    public static bool Req_IniciaProcessoGesDoc(long idRequerimento, string idGesDoc)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_IniciaProcessoGesDoc";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDocREQ", System.Data.SqlDbType.Int).Value = idRequerimento;
                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.VarChar).Value = idGesDoc;

                    con.Open();

                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    public static List<RequerimentoSimple> Req_ListarRequerimentos(string numero)
    {
        List<RequerimentoSimple> list = new List<RequerimentoSimple>();

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_ListarRequerimentos";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        RequerimentoSimple item = new RequerimentoSimple();

                        item.assunto = Convert.ToString(reader["assunto"]).Trim();
                        item.texto = Convert.ToString(reader["textoreq"]).Trim();
                        item.data = Convert.ToDateTime(reader["data"]);
                        item.tipo = Convert.ToString(reader["tipo"]).Trim();
                        item.estado = Convert.ToString(reader["estado"]).Trim();
                        item.estadoSigla = Convert.ToString(reader["estadoSigla"]).Trim();
                        item.email = Convert.ToString(reader["email"]).Trim();
                        item.idGesDoc = Convert.ToInt64(reader["idGesDoc"]);

                        list.Add(item);
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return list;
    }

    public static List<string> Req_ListarProcessosAtivos()
    {
        List<string> list = new List<string>();

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_ListarProcessosAtivos"; //GesDoc
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        list.Add(Convert.ToString(reader["processo"]).Trim());
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return list;
    }

    public static string Req_Get_Id_GesDoc(string processo)
    {
        string id = "-";

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_Get_Id_GesDoc";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@processo", System.Data.SqlDbType.VarChar).Value = processo;
                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToString(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = "-";
        }

        return id;
    }

    public static bool Req_TerminaProcessoGesDoc(string numero, string conclusao)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_TerminaProcessoGesDoc";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;
                    cmd.Parameters.Add("@conclusao", System.Data.SqlDbType.VarChar).Value = conclusao;

                    con.Open();

                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    public static int Req_CheckAnexo(int idGesDoc, string fileName)
    {
        int id = 0;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_CheckAnexo";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.Int).Value = idGesDoc;
                    cmd.Parameters.Add("@fileName", System.Data.SqlDbType.VarChar).Value = fileName;
                    cmd.Parameters.Add("@returnID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@returnID"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            id = 0;
        }

        return id;
    }

    public static string Req_Get_TipoRequerimento(string sigla)
    {
        string value = "-";

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_Get_TipoRequerimento";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@sigla", System.Data.SqlDbType.VarChar, 50).Value = sigla;
                    cmd.Parameters.Add("@returnValue", System.Data.SqlDbType.VarChar, 100).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    value = Convert.ToString(cmd.Parameters["@returnValue"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            value = "-";
        }

        return value;
    }

    public static bool ReqPermiteNovoProcessoGesDoc(string processo)
    {
        bool result = false;

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_PermiteNovoProcessoGesDoc";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDocReq", System.Data.SqlDbType.VarChar).Value = processo;
                    cmd.Parameters.Add("@returnValue", System.Data.SqlDbType.Bit).Direction = System.Data.ParameterDirection.Output;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    result = Convert.ToBoolean(cmd.Parameters["@returnValue"].Value.ToString());
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            result = false;
        }

        return result;
    }

    public static bool Req_IniciaProcessoDespachoSuperior(long idRequerimento, string texto)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_IniciaProcessoDespachoSuperior";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDocREQ", System.Data.SqlDbType.Int).Value = idRequerimento;
                    cmd.Parameters.Add("@texto", System.Data.SqlDbType.VarChar).Value = texto;

                    con.Open();

                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    public static bool Req_ConcluiProcessoDespachoSuperior(int id)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_ConcluiProcessoDespachoSuperior";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                    con.Open();

                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    public static List<RequerimentoDespachoSuperior> Req_ListarProcessosDespachoSuperior()
    {
        List<RequerimentoDespachoSuperior> listData = new List<RequerimentoDespachoSuperior>();

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Req_ListarProcessosDespachoSuperior"; //DespachoSuperior
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        RequerimentoDespachoSuperior item = new RequerimentoDespachoSuperior();

                        item.id = Convert.ToInt32(reader["id"]);
                        item.texto = Convert.ToString(reader["texto"]).Trim();
                        item.siglaDestinatario = Convert.ToString(reader["siglaDestinatario"]).Trim();
                        item.idArqde = Convert.ToString(reader["idArqde"]).Trim();
                        item.idGesDoc = Convert.ToString(reader["idGesDoc"]).Trim();
                        item.tipoRequerimento = Convert.ToString(reader["tipo"]).Trim();

                        listData.Add(item);
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return listData;
    }

    /**/
    public static Requerimento ReqGetPorProcesso(string idGesDoc)
    {
        List<Requerimento> list = new List<Requerimento>();

        try
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "ReqAcadListaParaProcesso";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idGesDoc", System.Data.SqlDbType.BigInt).Value = idGesDoc;

                    con.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Requerimento item = new Requerimento();

                        item.mensagem = Convert.ToString(reader["comment"]).Trim();
                        item.codCurso = Convert.ToString(reader["curso"]).Trim();
                        item.data = Convert.ToDateTime(reader["dataEstado"]);
                        item.idGesDoc = Convert.ToString(reader["idGesDoc"]).Trim();
                        item.numero = Convert.ToString(reader["numero"]).Trim();
                        item.refRequerimento = Convert.ToString(reader["ref"]).Trim();
                        item.estadoSigla = Convert.ToString(reader["sigla"]).Trim();
                        item.iTypeID = Convert.ToInt32(reader["iTypeID"]);
                        item.nome = Convert.ToString(reader["nomeAluno"]).Trim();
                        item.nomeCurso = Convert.ToString(reader["nomeCurso"]).Trim();
                        item.estado = Convert.ToString(reader["estado"]).Trim();
                        item.tipo = Convert.ToString(reader["tipo"]).Trim();
                        item.assunto = Convert.ToString(reader["assunto"]).Trim();
                        item.email = Convert.ToString(reader["email"]).Trim();

                        list.Add(item);
                    }
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return list.OrderByDescending(r => r.data).First();
    }

}